package com.ProgettoFinale.model;

import java.util.ArrayList;

public class Ordine {
	
	private int ordineId;
	private long dataOrdine;
	private String codiceOrdine;
	private Utente utenteRef;
	private ArrayList<Oggetto> listaOggetti = new ArrayList<Oggetto>();
	
	public Ordine() {
		
	}
	public Ordine(long dataOrdine, String codiceOrdine, Utente utenteRef) {
	
		this.dataOrdine = dataOrdine;
		this.codiceOrdine = codiceOrdine;
		this.utenteRef = utenteRef;
	}
	public int getOrdineId() {
		return ordineId;
	}
	public void setOrdineId(int ordineId) {
		this.ordineId = ordineId;
	}
	public long getDataOrdine() {
		return dataOrdine;
	}
	public void setDataOrdine(long dataOrdine) {
		this.dataOrdine = dataOrdine;
	}
	public String getCodiceOrdine() {
		return codiceOrdine;
	}
	public void setCodiceOrdine(String codiceOrdine) {
		this.codiceOrdine = codiceOrdine;
	}
	
	public Utente getUtente() {
		return utenteRef;
	}
	public void setUtente(Utente utente) {
		this.utenteRef = utente;
	}
	@Override
	public String toString() {
		return "Ordine [ordineId=" + ordineId + ", dataOrdine=" + dataOrdine + ", codiceOrdine=" + codiceOrdine
				+ ", utenteRef=" + utenteRef + "]";
	}
	
	
	
	
	

}


