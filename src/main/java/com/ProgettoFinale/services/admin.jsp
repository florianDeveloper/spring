<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%
HttpSession sessione = request.getSession();
String tipologiaUtente = (String) sessione.getAttribute("tipologia");
if (!tipologiaUtente.equals("admin"))
	response.sendRedirect("errore.jsp?tipo_errore=NOT_ALLOWED");
%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<title>Pagina dell'admin</title>
</head>
<body>
	<style>
.active {
	font-weight: bolder;
}

.active a {
	color: blue !important;
}
</style>

	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="admin.jsp">ADMIN</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarNav" aria-controls="navbarNav"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNav">
			<ul class="navbar-nav">
				<li class="nav-item active"><a class="nav-link"
					href="admin.jsp">Elenco Prodotti</a></li>
				<li class="nav-item"><a class="nav-link" href="adminUtenti.jsp">Elenco
						Utenti</a></li>
				<li class="nav-item"><a class="nav-link" href="adminOrdini.jsp">Elenco
						Ordini</a></li>
				<li class="nav-item"><a class="nav-link" href="logout">LogOut</a>
				</li>
			</ul>
		</div>
	</nav>

	<div class="container">


		<div class="row mt-5">
			<div class="col">
				<h1>Lista prodotti</h1>
				<p>Di seguito puoi consultare tutta la lista dei prodotti</p>
			</div>
		</div>

		<div class="row mt-5">
			<div class="col">

				<table class="table">
					<thead>
						<tr>
							<th>Nome prodotto</th>
							<th>Descrizione</th>
							<th COLSPAN="2">Azioni</th>
						</tr>
					</thead>
					<tbody id="contenuto-elenco">

					</tbody>
				</table>

			</div>
		</div>

		<div class="row mt-5">
			<div class="col">
				<button type="button" class="btn btn-primary"
					id="btn_inserisci_oggetto">Inserisci nuovo prodotto</button>
			</div>
		</div>





		<!--  Modale inserimento: START  -->
		<div class="modal fade" id="modaleInserimento" tabindex="-1"
			role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Inserisci
							nuovo prodotto</h5>
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<p>Riempi i campi sottostanti per inserire un nuovo prodotto</p>
						<div class="form-group">
							<label for="input_nome">Nome prodotto:</label> <input type="text"
								class="form-control" id="input_nome" />
						</div>

						<div class="form-group">
							<label for="input_descrizione">Descrizione:</label>
							<textarea type="text" class="form-control" id="input_descrizione"></textarea>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" id="cta-inserisci">Inserisci</button>
					</div>
				</div>
			</div>
		</div>
		<!--  Modale inserimento: END  -->

		<!--  Modale modifica: START  -->
		<div class="modal fade" id="modaleModifica" data-identificatore=""
			tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
			aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Aggiorna
							prodotto</h5>
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<p>Riempi i campi sottostanti per modificare un prodotto
							esistente</p>
						<div class="form-group">
							<label for="nome_edit">Nome prodotto:</label> <input type="text"
								class="form-control" id="nome_edit" />
						</div>

						<div class="form-group">
							<label for="descrizione_edit">Descrizione:</label>
							<textarea type="text" class="form-control" id="descrizione_edit"></textarea>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-success" id="cta-modifica">Modifica</button>
					</div>
				</div>
			</div>
		</div>
		<!--  Modale modifica: END  -->







		<script src="https://code.jquery.com/jquery-3.6.0.js"
			integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
			crossorigin="anonymous"></script>
		<script
			src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
			integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
			crossorigin="anonymous"></script>
		<script
			src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
			integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
			crossorigin="anonymous"></script>
		<script src="js/admin.js"></script>
</body>
</html>